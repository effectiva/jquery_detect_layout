# Detect_layout script

## Overview
Detects layouts on resize or orientation change ``(by watching z-index on
html element controlled by css media queries)`` and triggers a custom event on window element.


## Usage:

```js
$(window).on('responsive', function(){
    console.log('Layout changed!');
});
```
#### or

```js
$(window).on('responsive:sm', my_function);
```

### Required CSS rules
> You can control when will layout change trigger by setting z-index at appropriate window size.

> Media queries are set up to follow bootstrap's breakpoints.
>For more information about breakpoints refer to: [Bootstrap documentation](http://getbootstrap.com/css/#grid-options)

```css
/*  Default media queries  */
@media(max-width: 480px)  { html { z-index: 480; }  } /*xxs*/
@media(min-width: 481px)  { html { z-index: 481; }  } /*xs*/
@media(min-width: 768px)  { html { z-index: 768; }  } /*sm*/
@media(min-width: 992px)  { html { z-index: 992; }  } /*md*/
@media(min-width: 1200px) { html { z-index: 1200; } } /*lg*/
```
>__When adding new queries it is recommended to set ``z-index`` value to match viewport width in pixels__


### Events

| Event Type            | Description                                                |
| -------------         | -------------                                              |
| resize:end            | This event is triggered on window resize end.              |
| responsive:``[name]`` | This event is triggered when html index becomes ``[size]`` |

>``[name]`` => layout name defined in config or custom added value.  Example: ``responsive:sm``

>``[size]`` => layout size defined in config or custom added value.  Example: 992

***

### Default javascript config
```js
  $.DetectLayout = {
    config: {
      480: 'xxs',
      481: 'xs',
      768: 'sm',
      992: 'md',
      1200: 'lg'
    }
  };
```

###__! NOTE__
>You can add more layout changes if needed.

>After adding wanted css media query and setting z-index value, you need to extend default config object in javascript


>__IMPORTANT__ It is required to add custom layouts before detect_layout.js is initialized.

>___Check Step 2 for detailed instructions___

## Adding custom layout change: 

### Step 1: CSS

```css
@media(max-width: 480px)  { html { z-index: 480;  }  } /*xxs*/
@media(min-width: 481px)  { html { z-index: 481;  }  } /*xs*/
@media(min-width: 640px)  { html { z-index: 640;  }  } /*malcolm*/ /*** custom breakpoint ***/
@media(min-width: 768px)  { html { z-index: 768;  }  } /*sm*/
@media(min-width: 992px)  { html { z-index: 992;  }  } /*md*/
@media(min-width: 1200px) { html { z-index: 1200; }  } /*lg*/
@media(min-width: 1600px) { html { z-index: 1600; }  } /*mrWhite*/ /*** custom breakpoint ***/
``` 
> When adding custom media query pay attention where you are placing it so it doesn't get overriden by default queries.

### Step 2: Javascript    

```js
  (function(){
    'use strict';
    window.DetectLayoutConfig = {
     640: 'malcolm', // custom layout example
     1600: 'mrWhite'  // custom layout example
     // add more layouts if needed
    };
  })();
```
> ####__! IMPORTANT__
Extend layouts in a separate file which is included before detect_layout.js script.
That way your custom layouts are recognized on window load event.


Example how to include script files in index.html
```html
  <html>
  <head>
    <script src="/bower_components/jquery/dist/jquery.js"></script>
    <script src="/detect_layout_config.js"></script> <!-- Script with custom layouts -->
    <script src="/detect_layout.js"></script>
    <script src="app.js"></script>
    ......

```

__TIP__ `If you are adding custom layouts don't edit detect_layout.js, instead add your layouts in a separate javascript file.`

Once you add your layout change you also have an event for that layout:

```js

$(window).on('responsive:malcolm', function(){
    console.log('Malcolm in the middle layout!');
});

$(window).on('responsive:mrWhite', function(){
    console.log('Say my name');
});

```

### DEMO
For more information about usage see provided example.

In order to be able to try example you need to do the following:

  + ``git clone`` this repository
  + run ``bower install``
  + run ``http-server``

### Install instructions
__To install latest version run:__

``bower install https://git@bitbucket.org/effectiva/jquery_detect_layout.git#master``

__To install stable release version run:__

``bower install https://git@bitbucket.org/effectiva/jquery_detect_layout.git``
