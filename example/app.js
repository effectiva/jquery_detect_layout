$(function(){
  'use strict';

  $(window).on('responsive', function(e, layout){
    $('.title').html(layout.name + ': ' + layout.size);
    var msg = '';

    if(layout.initial){
      msg = 'Initial detect on page load!<br>';
      console.log('Initial detect on page load!', layout.name, 'with size', layout.size);
    }else{
      msg = 'Layout changed!<br>';
      console.log('Layout changed!', layout.name, 'with size', layout.size);
    }

    if(layout.size <= 1200){
      msg += 'Smaller or equal to 1200<br>';
      console.log('Smaller or equal to 1200');
    }

    $('.desc').html(msg);
    console.log('Detected layout is stored in $.DetectLayout.current: ', $.DetectLayout.current);
  });

  $(window).on('responsive:sm', function(e, layout){
    console.log('This will trigger only on sm', layout);
  });

});
